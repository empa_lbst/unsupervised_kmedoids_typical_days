# README #

### General description

The typical day method used in Dominguez-Munoz et al. (2011) are implemented in a MATLAB code. The k-medoids clustering method is employed selecting k typical days able to represent the full year demand pro
file. 3 peaks days for heating, cooling and electricity demand pro
files are added. The k number of days is selected by the user.
The unsupervised typical days function select the number k of typical days which best represents the full year demand profile. This is done based on a minimization of error between the load duration curves for heating, cooling and electricity and the Davies-Bouldin index representing the intra and inter clusters relation (Davies and Bouldin (1979)). The user select the number k of typical days depending on the pareto front and the computation time.

### Set-up
* We assume you have MATLAB 2014b or upward.

### Usage
Main function is NonNaive_Optim.m allowing to run within boundaries the genetic algorithm and output pareto front plot.

### More information

A more detailed model description is available in the "Guidelines_Non_naive_typ_days.pdf".

### Authors

Julien Marquant, Georgios Mavromatidis