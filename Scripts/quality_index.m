function i_db = quality_index(C,D,k)
%% Compute error in Load Duration Curve between clustered and original data
% in selection of typical days package
%
% Required parameters:
% - D: Distance matrix
% - C: Typical days matrix
% - k: number of clusters (without peak days)


%% Dispersion measure of Cluster i : Si
q=2;     % Euclidean distance 
S=zeros(1,k);

for i = 1:1:k
    %D is an n-by-k matrix, where element (j,m) is the distance from observation j to medoid m.
    % sumd=sum(D(idx==i,i))
    S(1,i)=((1/sum(idx == i))*sum((D(idx==i,i)).^q))^(1/q);
end

%% Distance between medoids
t=2;     % Euclidean distance
M=zeros(k,k); % Medoids center matrix
R=zeros(k,k); % Compactness matrix between two cluster
MaxR=zeros(1,k);

for i=1:1:k
    for j=1:1:k
        if i==j
            M(i,j)=1;
        else
            M(i,j)=(sum((abs(C(i,1:72)-C(j,1:72)))^t))^(1/t);
        end
        R(i,j)=(S(1,i)+S(1,j))/M(i,j);
    end
    MaxR(1,i)=max(R(i,:));
end

i_db=(1/k)*sum(MaxR);