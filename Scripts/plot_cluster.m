%% function to plot cluster
% cluster assignments and medoids
figure;
plot(L(idx==1,1),L(idx==1,2),'r.','MarkerSize',7)
hold on
plot(L(idx==2,1),L(idx==2,2),'b.','MarkerSize',7)
plot(C(:,1),C(:,2),'co',...
     'MarkerSize',7,'LineWidth',1.5)
legend('Cluster 1','Cluster 2','Medoids',...
       'Location','NW');
title('Cluster Assignments and Medoids');
hold off