%% UNSUPERVISED K-MEDOIDS CLUSTERING ALGORITHM 
% -------------------------------------------------------------------------
% Script: 
% Function to be called in order to evaluate the best number of k-days
% representing at best a full year of hourly demand profiles
%
% k = number of typical days apart from the three peak days for heating,
%     cooling and electricity
% Run NonNaiveOptim for K from lb= 2 to ub=50
% Matlab 2014b required for kmedoids function call
% -------------------------------------------------------------------------
% Aim: 
% Function to call and set-up the GA and plot the resulting Pareto front 
% to select the best k-number of typical days for the k-medoids method
% Multi-Objective Optimisation minimizing:
%         - Davies-Bouldin index (cluster separation measure)
%         - ELDC (Error between Load Duration Curves)
% -------------------------------------------------------------------------
% Input: 
% Heat_vec = (8760 x 1) vector of heating demand at hourly resolution
% Cool_vec = (8760 x 1) vector of cooling demand at hourly resolution
% Elec_vec = (8760 x 1) vector of electricity demand at hourly resolution
%
% -------------------------------------------------------------------------
% Data:
% Change data in following folder:
% Heat_data='data\Heat.mat';                           % hourly heating demand
% Cool_data='data\Cool.mat';                           % hourly cooling demand
% Elec_data='data\Elec.mat';                           % hourly elec demand
% 
% 
% -------------------------------------------------------------------------
% Output: 
% [x,a]:
% x = number of typical days 
% a(:,2): error in load duration curve
% a(:,3): Davies-Bouldin index
% 
% -------------------------------------------------------------------------
% Example of use:
% evaluate the best number of k-days
% representing at best a full year of hourly demand profiles
% 
% -------------------------------------------------------------------------
% References: 
% 
% -------------------------------------------------------------------------
% Written by Julien Marquant and Georgios Mavromatidis
% -------------------------------------------------------------------------


function [x,a] = NonNaive_Optim()


h = @(x) Typical_day_GA(x);

number_variables=1;             % k+3: number of typical k + cool/heat/elec peaks  
lb=2;
ub=50;
Bound=[lb;ub];                  % bound for k
                        

options = gaoptimset( ...
    'PopInitRange',Bound,'Display','iter','StallGenL',40,'Generations',150, ...
    'PopulationSize',60,'PlotFcns',{@gaplotbestf,@gaplotbestindiv,@gaplotpareto});
[x,f,exitflag] = gamultiobj(h,number_variables,[],[],[],[],lb,ub,options);

% Plot graph overview ELDC + DB index
figure;
a=[round(x),f];

% plot k number of typical days
x = a(:,1);

% plot ELDC
y = a(:,2);
% plot DB index
y2=a(:,3);
ax1 = subplot(2,1,1);
scatter(ax1,x,y)

ax2 = subplot(2,1,2);
scatter(ax2,x,y2,'filled','d')

%'CreationFcn',@int_pop,'MutationFcn',@int_mutation