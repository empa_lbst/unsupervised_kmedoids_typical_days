function [ eldc_saved, i_db_saved, count, typical__vs_original_days, typical_days,idx,Elec_peak_day,Heat_peak_day,Cool_peak_day] = NonNaive_typical_day(SFH100, k)
% Function to create typical days according to the procedure described in 
% 'Selection of typical days for CHP optimization'

% Heat_vec = (8760 x 1) vector of heating demand at hourly resolution
% Cool_vec = (8760 x 1) vector of cooling demand at hourly resolution
% Elec_vec = (8760 x 1) vector of electricity demand at hourly resolution

% k = number of typical days apart from the three peak days for heating,
%     cooling and electricity
%eva = evalclusters(D,C,'DaviesBouldin')
%%
eldc_saved=[];
i_db_saved=[];
% kmax=30;

% for k=1:1:kmax

%  Cool_vec = load(Cool_data);
%  Elec_vec = load(Elec_data);
%  Heat_vec = load(Heat_data);
 
 Cool_vec = SFH100(:,3);
 Elec_vec = SFH100(:,1);
 Heat_vec = SFH100(:,2);

%% Reshape the matrices
    
Heat = reshape(Heat_vec, 24, 365)';
Cool = reshape(Cool_vec, 24, 365)';
Elec = reshape(Elec_vec, 24, 365)';

%% Exclude peak days

% Find peak days and remove them from the dataset
Heat_peak_hour = find(Heat_vec == max(Heat_vec)); % location of max in vector
Cool_peak_hour = find(Cool_vec == max(Cool_vec)); % location of max in vector
Elec_peak_hour = find(Elec_vec == max(Elec_vec)); % location of max in vector

Heat_peak_day = fix(Heat_peak_hour/24) + 1; % peak day for heating
Cool_peak_day = fix(Cool_peak_hour/24) + 1; % peak day for cooling
Elec_peak_day = fix(Elec_peak_hour/24) + 1; % peak day for electricity

[n,~]=size(Heat_peak_day);
if n>1
    Heat_peak_day=Heat_peak_day(1,1);
end
[n,~]=size(Cool_peak_day);
if n>1
    Cool_peak_day=Cool_peak_day(1,1);
end
[n,~]=size(Elec_peak_day);
if n>1
    Elec_peak_day=Elec_peak_day(1,1);
end

%% Horizontally concatenate matrices

L = horzcat(Heat, Cool, Elec);

Heat_peak_profile = L(Heat_peak_day,:); % Saving the peak heating day load profile
Cool_peak_profile = L(Cool_peak_day,:); % Saving the peak cooling day load profile
Elec_peak_profile = L(Elec_peak_day,:); % Saving the peak electricity day load profile

%% Remove peak days

if (Heat_peak_day ~= Cool_peak_day) & (Heat_peak_day ~= Elec_peak_day)
   L = removerows(L, [Heat_peak_day Cool_peak_day Elec_peak_day]);
end

%% Perform the clustering

[idx,C,sumd,D,midx,info] = kmedoids(L,k,'Distance','euclidean');

%% Calculation of scaling factors

count = ones(k,1); % preallocation for speed
store_scalingsys=[];

for i = 1:1:k
    
    enum_h = sum(sum(L(idx==i,1:24)));
    denom_h = sum(C(i,1:24)) * sum(idx == i);
    enum_c = sum(sum(L(idx==i,25:48)));
    denom_c = sum(C(i,25:48)) * sum(idx == i);
    enum_e = sum(sum(L(idx==i,49:72)));
    denom_e = sum(C(i,49:72)) * sum(idx == i);
    
    C(i,1:24) = C(i,1:24) * (enum_h / denom_h);
    C(i,25:48) = C(i,25:48) * (enum_c / denom_c);
    C(i,49:72) = C(i,49:72) * (enum_e / denom_e);
    
    count(i) = sum(idx == i); % How many days each typical day represents
    store_scalingsys=[store_scalingsys,[enum_h;denom_h;enum_c;denom_c;enum_e;denom_e]];
    
end
C(isnan(C)) = 0;
typical_days = [C; Heat_peak_profile; Cool_peak_profile; Elec_peak_profile];

typical_days(isnan(typical_days)) = 0;

typical_days_heat = reshape(typical_days(:,1:24)',24*(k+3),1);
typical_days_cool = reshape(typical_days(:,25:48)',24*(k+3),1);
typical_days_elec = reshape(typical_days(:,49:72)',24*(k+3),1);


%% Compute error in Load Duration Curve between clustered and original data
% in selection of typical days package
%
% Required parameters:
% - orginal data
% - count 
% - series
% - typical_days (L = horzcat(Heat, Cool, Elec);)


%% Load duration curve calculation for clustered data
% Reconstitue full year from clustered data
typical_days_year=typical_days;
for i = 1:1:k
    size_cluster = count(i,1);
    for j = 1:1:size_cluster-1
        typical_days_year=[typical_days_year;typical_days(i,:)];
    end
end

%Respective load factor
Heating_typical_days = reshape(typical_days_year(:,1:24),1,8760)';
Cooling_typical_days = reshape(typical_days_year(:,25:48),1,8760)';                 % number of clusters = k + 3(heat/cooling/electricity peaks)
Elec_typical_days = reshape(typical_days_year(:,49:72),1,8760)';



% Reconstiture original data 
% data_original = [L;Heat_peak_profile;Cool_peak_profile;Elec_peak_profile];
% 
% Heating_typical_day = reshape(typical_days(:,1:24),1,(k+3)*24);
% Cooling_typical_day = reshape(typical_days(:,25:48),1,(k+3)*24);                 % number of clusters = k + 3(heat/cooling/electricity peaks)
% Elec_typical_day = reshape(typical_days(:,49:72),1,(k+3)*24);


% Sort data
%Clustered data
load_c_heat = sort(Heating_typical_days,'descend');
load_c_cool = sort(Cooling_typical_days,'descend');
load_c_elec = sort(Elec_typical_days,'descend');

%Orginal data
load_o_heat = sort(Heat_vec,'descend');
load_o_cool = sort(Cool_vec,'descend');
load_o_elec = sort(Elec_vec,'descend');

%% PLOT LDC
% heat_plot = duration_plot([Heat_vec, Heating_typical_days],'y_label','Heating demand_matlab');
% cool_plot = duration_plot([Cool_vec, Cooling_typical_days],'y_label','Cooling demand_matlab');
% elec_plot = duration_plot([Elec_vec, Elec_typical_days],'y_label','Electricity demand_matlab');


%% Error in load duration curve calculation: HEATING
eldc_heat = sum(abs(load_o_heat-load_c_heat))/sum(load_o_heat);

%% Error in load duration curve calculation: COOLING
eldc_cool = sum(abs(load_o_cool-load_c_cool))/sum(load_o_cool);

%% Error in load duration curve calculation: ELECTRICITY
eldc_elec = sum(abs(load_o_elec-load_c_elec))/sum(load_o_elec);

eldc = eldc_heat + eldc_cool + eldc_elec;

eldc_saved = [eldc_saved;eldc];


%% Compute error in Load Duration Curve between clustered and original data
% in selection of typical days package
%
% Required parameters:
% - D: Distance matrix
% - C: Typical days matrix
% - k: number of clusters (without peak days)


%% Dispersion measure of Cluster i : Si
q=2;     % Euclidean distance 
S=zeros(1,k);

for i = 1:1:k
    %D is an n-by-k matrix, where element (j,m) is the distance from observation j to medoid m.
    % sumd=sum(D(idx==i,i))
    S(1,i)=((1/sum(idx == i))*sum((D(idx==i,i)).^q))^(1/q);
end

%% Distance between medoids
t=2;     % Euclidean distance
M=zeros(k,k); % Medoids center matrix
R=zeros(k,k); % Compactness matrix between two clusters
MaxR=zeros(1,k);

for i=1:1:k
    for j=1:1:k
        if i==j
            M(i,j)=1;
            R(i,j)=0;
        else
            M(i,j)=(sum((abs(C(i,1:72)-C(j,1:72))).^t))^(1/t);
            R(i,j)=(S(1,i)+S(1,j))/M(i,j);
        end
    end
    MaxR(1,i)=max(R(i,:));
end

i_db=(1/k)*sum(MaxR);

i_db_saved=[i_db_saved;i_db];

% end
% figure
% plot(1:1:kmax, eldc_saved)
% 
% figure
% plot(1:1:kmax, i_db_saved)

typical__vs_original_days = [typical_days_heat; typical_days_cool; typical_days_elec; Heat_vec; Cool_vec; Elec_vec];

end