function [ typical_days_heat, typical_days_cool, typical_days_elec, count, eldc_saved ] = NonNaive_typical_day_trial(Heat_vec, Cool_vec, Elec_vec)
% Function to create typical days according to the procedure described in 
% 'Selection of typical days for CHP optimization'

% Heat_vec = (8760 x 1) vector of heating demand at hourly resolution
% Cool_vec = (8760 x 1) vector of cooling demand at hourly resolution
% Elec_vec = (8760 x 1) vector of electricity demand at hourly resolution

% k = number of typical days apart from the three peak days for heating,
%     cooling and electricity
eldc_saved=[];
k=4;
%for k=1:1:20

%% Reshape the matrices

Heat_vec=Heating_trial;
Cool_vec=Cooling_trial;
    
Heat = reshape(Heat_vec, 1, 10)';
Cool = reshape(Cool_vec, 1, 10)';


%% Horizontally concatenate matrices

L = horzcat(Heat, Cool);


%% Remove peak days

%% Perform the clustering

[idx,C,sumd,D,midx,info] = kmedoids(L,k,'Distance','euclidean','Algorithm','pam');

%% Calculation of scaling factors

count = ones(k,1); % preallocation for speed

store_scalingsys=[];

for i = 1:1:k
    
    enum_h = sum(sum(L(idx==i,1)));
    denom_h = sum(C(i,1)) * sum(idx == i);
    enum_c = sum(sum(L(idx==i,2)));
    denom_c = sum(C(i,2)) * sum(idx == i);
    
    C(i,1) = C(i,1) * (enum_h / denom_h);
    C(i,2) = C(i,2) * (enum_c / denom_c);
    
    count(i) = sum(idx == i); % How many days each typical day represents

    store_scalingsys=[store_scalingsys,[enum_h;denom_h;enum_c;denom_c]];
    
end

typical_days = [C];

typical_days(isnan(typical_days)) = 0;

typical_days_heat = reshape(typical_days(:,1)',1*(k),1);
typical_days_cool = reshape(typical_days(:,2)',1*(k),1);


%% Compute error in Load Duration Curve between clustered and original data
% in selection of typical days package
%
% Required parameters:
% - orginal data
% - count 
% - series
% - typical_days (L = horzcat(Heat, Cool, Elec);)


%% Load duration curve calculation for clustered data
% Reconstitue full year from clustered data
typical_days_year=typical_days;
for i = 1:1:k
    size_cluster = count(i,1);
    for j = 1:1:size_cluster-1
        typical_days_year=[typical_days_year;typical_days(i,:)];
    end
end

%Respective load factor
Heating_typical_days = reshape(typical_days_year(:,1:24),1,8760)';
Cooling_typical_days = reshape(typical_days_year(:,25:48),1,8760)';                 % number of clusters = k + 3(heat/cooling/electricity peaks)
Elec_typical_days = reshape(typical_days_year(:,49:72),1,8760)';



% Reconstiture original data 
% data_original = [L;Heat_peak_profile;Cool_peak_profile;Elec_peak_profile];
% 
% Heating_typical_day = reshape(typical_days(:,1:24),1,(k+3)*24);
% Cooling_typical_day = reshape(typical_days(:,25:48),1,(k+3)*24);                 % number of clusters = k + 3(heat/cooling/electricity peaks)
% Elec_typical_day = reshape(typical_days(:,49:72),1,(k+3)*24);


% Sort data
%Clustered data
load_c_heat = sort(Heating_typical_days,'descend');
load_c_cool = sort(Cooling_typical_days,'descend');
load_c_elec = sort(Elec_typical_days,'descend');

%Orginal data
load_o_heat = sort(Heat_vec,'descend');
load_o_cool = sort(Cool_vec,'descend');
load_o_elec = sort(Elec_vec,'descend');


heat_plot = duration_plot([Heat_vec, Heating_typical_days],'y_label','Heating demand_matlab');
cool_plot = duration_plot([Cool_vec, Cooling_typical_days],'y_label','Cooling demand_matlab');
elec_plot = duration_plot([Elec_vec, Elec_typical_days],'y_label','Electricity demand_matlab');


%% Error in load duration curve calculation: HEATING
eldc_heat = sum(abs(load_o_heat-load_c_heat))/sum(load_o_heat);

%% Error in load duration curve calculation: COOLING
eldc_cool = sum(abs(load_o_cool-load_c_cool))/sum(load_o_cool);

%% Error in load duration curve calculation: ELECTRICITY
eldc_elec = sum(abs(load_o_elec-load_c_elec))/sum(load_o_elec);

eldc = eldc_heat + eldc_cool + eldc_elec;

eldc_saved = [eldc_saved;eldc];

%end
% figure
% plot(1:1:20, eldc_saved)

%end