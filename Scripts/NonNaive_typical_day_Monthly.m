function [ eldc_saved, i_db_saved, count, typical__vs_original_days, typical_days,idx,Elec_peak_day,Heat_peak_day,Cool_peak_day] = NonNaive_typical_day(SFH100, k)
% Function to create typical days according to the procedure described in 
% 'Selection of typical days for CHP optimization'

% Heat_vec = (8760 x 1) vector of heating demand at hourly resolution
% Cool_vec = (8760 x 1) vector of cooling demand at hourly resolution
% Elec_vec = (8760 x 1) vector of electricity demand at hourly resolution

% k = number of typical days apart from the three peak days for heating,
%     cooling and electricity
%eva = evalclusters(D,C,'DaviesBouldin')
%%
eldc_saved=[];
i_db_saved=[];
% kmax=30;

% for k=1:1:kmax

%  Cool_vec = load(Cool_data);
%  Elec_vec = load(Elec_data);
%  Heat_vec = load(Heat_data);
 
 Cool_vec = SFH100(:,2);
 Elec_vec = SFH100(:,1);
 Heat_vec = SFH100(:,3);

%% Reshape the matrices

Cool_vec1 = Cool_vec(1:(24*31),1);
Elec_vec1 = Elec_vec(1:(24*31),1);
Heat_vec1 = Heat_vec(1:(24*31),1);

Cool_vec2 = Cool_vec(745:(1416),1);
Elec_vec2 = Elec_vec(745:(1416),1);
Heat_vec2 = Heat_vec(745:(1416),1);

Cool_vec3 = Cool_vec(1417:(2160),1);
Elec_vec3 = Elec_vec(1417:(2160),1);
Heat_vec3 = Heat_vec(1417:(2160),1);

Cool_vec4 = Cool_vec(2161:(2880),1);
Elec_vec4 = Elec_vec(2161:(2880),1);
Heat_vec4 = Heat_vec(2161:(2880),1);

Cool_vec5 = Cool_vec(2881:(3624),1);
Elec_vec5 = Elec_vec(2881:(3624),1);
Heat_vec5 = Heat_vec(2881:(3624),1);

Cool_vec6 = Cool_vec(3625:(4344),1);
Elec_vec6 = Elec_vec(3625:(4344),1);
Heat_vec6 = Heat_vec(3625:(4344),1);

Cool_vec7 = Cool_vec(4345:(5088),1);
Elec_vec7 = Elec_vec(4345:(5088),1);
Heat_vec7 = Heat_vec(4345:(5088),1);

Cool_vec8 = Cool_vec(5089:(5832),1);
Elec_vec8 = Elec_vec(5089:(5832),1);
Heat_vec8 = Heat_vec(5089:(5832),1);

Cool_vec9 = Cool_vec(5833:(6552),1);
Elec_vec9 = Elec_vec(5833:(6552),1);
Heat_vec9 = Heat_vec(5833:(6552),1);

Cool_vec10 = Cool_vec(6553:(7296),1);
Elec_vec10 = Elec_vec(6553:(7296),1);
Heat_vec10 = Heat_vec(6553:(7296),1);

Cool_vec11 = Cool_vec(7297:(8016),1);
Elec_vec11 = Elec_vec(7297:(8016),1);
Heat_vec11 = Heat_vec(7297:(8016),1);

Cool_vec12 = Cool_vec(8017:(8760),1);
Elec_vec12 = Elec_vec(8017:(8760),1);
Heat_vec12 = Heat_vec(8017:(8760),1);

% January
Heat1 = reshape(Heat_vec1, 24, 31)';
Cool1 = reshape(Cool_vec1, 24, 31)';
Elec1 = reshape(Elec_vec1, 24, 31)';

% February
Heat2 = reshape(Heat_vec2, 24, 28)';
Cool2 = reshape(Cool_vec2, 24, 28)';
Elec2 = reshape(Elec_vec2, 24, 28)';

% March
Heat3 = reshape(Heat_vec3, 24, 31)';
Cool3 = reshape(Cool_vec3, 24, 31)';
Elec3 = reshape(Elec_vec3, 24, 31)';

% Abril
Heat4 = reshape(Heat_vec4, 24, 30)';
Cool4 = reshape(Cool_vec4, 24, 30)';
Elec4 = reshape(Elec_vec4, 24, 30)';

% May
Heat5 = reshape(Heat_vec5, 24, 31)';
Cool5 = reshape(Cool_vec5, 24, 31)';
Elec5 = reshape(Elec_vec5, 24, 31)';

% June 
Heat6 = reshape(Heat_vec6, 24, 30)';
Cool6 = reshape(Cool_vec6, 24, 30)';
Elec6 = reshape(Elec_vec6, 24, 30)';

% July
Heat7 = reshape(Heat_vec7, 24, 31)';
Cool7 = reshape(Cool_vec7, 24, 31)';
Elec7 = reshape(Elec_vec7, 24, 31)';

% August
Heat8 = reshape(Heat_vec8, 24, 31)';
Cool8 = reshape(Cool_vec8, 24, 31)';
Elec8 = reshape(Elec_vec8, 24, 31)';

% September
Heat9 = reshape(Heat_vec9, 24, 30)';
Cool9 = reshape(Cool_vec9, 24, 30)';
Elec9 = reshape(Elec_vec9, 24, 30)';

% October
Heat10 = reshape(Heat_vec10, 24, 31)';
Cool10 = reshape(Cool_vec10, 24, 31)';
Elec10 = reshape(Elec_vec10, 24, 31)';

% November
Heat11 = reshape(Heat_vec11, 24, 30)';
Cool11 = reshape(Cool_vec11, 24, 30)';
Elec11 = reshape(Elec_vec11, 24, 30)';

% December
Heat12 = reshape(Heat_vec12, 24, 31)';
Cool12 = reshape(Cool_vec12, 24, 31)';
Elec12 = reshape(Elec_vec12, 24, 31)';


Heat=[Heat1;Heat2;Heat3;Heat4;Heat5;Heat6;Heat7;Heat8;Heat9;Heat10;Heat11;Heat12];
Cool=[Cool1;Cool2;Cool3;Cool4;Cool5;Cool6;Cool7;Cool8;Cool9;Cool10;Cool11;Cool12];
Elec=[Elec1;Elec2;Elec3;Elec4;Elec5;Elec6;Elec7;Elec8;Elec9;Elec10;Elec11;Elec12];

field1 = 'Heat';
value1 = {{Heat1,Heat2,Heat3,Heat4,Heat5,Heat6,Heat7,Heat8,Heat9,Heat10,Heat11,Heat12}};
field2 = 'Cool';
value2 = {{Cool1,Cool2,Cool3,Cool4,Cool5,Cool6,Cool7,Cool8,Cool9,Cool10,Cool11,Cool12}};
field3 = 'Elec';
value3 = {{Elec1,Elec2,Elec3,Elec4,Elec5,Elec6,Elec7,Elec8,Elec9,Elec10,Elec11,Elec12}};
Demand=struct(field1,value1,field2,value2,field3,value3);

% %% Exclude peak days
% 
% % Find peak days and remove them from the dataset
% Heat_peak_hour = find(Heat_vec == max(Heat_vec)); % location of max in vector
% Cool_peak_hour = find(Cool_vec == max(Cool_vec)); % location of max in vector
% Elec_peak_hour = find(Elec_vec == max(Elec_vec)); % location of max in vector
% 
% Heat_peak_day = fix(Heat_peak_hour/24) + 1; % peak day for heating
% Cool_peak_day = fix(Cool_peak_hour/24) + 1; % peak day for cooling
% Elec_peak_day = fix(Elec_peak_hour/24) + 1; % peak day for electricity
% 
% [n,~]=size(Heat_peak_day);
% if n>1
%     Heat_peak_day=Heat_peak_day(1,1);
% end
% [n,~]=size(Cool_peak_day);
% if n>1
%     Cool_peak_day=Cool_peak_day(1,1);
% end
% [n,~]=size(Elec_peak_day);
% if n>1
%     Elec_peak_day=Elec_peak_day(1,1);
% end

%% Horizontally concatenate matrices
typical_days_monthly=[];
for j=1:1:12
    
    L = horzcat(Demand.Heat{1,j}, Demand.Cool{1,j}, Demand.Elec{1,j});

% Heat_peak_profile = L(Heat_peak_day,:); % Saving the peak heating day load profile
% Cool_peak_profile = L(Cool_peak_day,:); % Saving the peak cooling day load profile
% Elec_peak_profile = L(Elec_peak_day,:); % Saving the peak electricity day load profile
% 
% %% Remove peak days
% 
% if (Heat_peak_day ~= Cool_peak_day) & (Heat_peak_day ~= Elec_peak_day)
%    L = removerows(L, [Heat_peak_day Cool_peak_day Elec_peak_day]);
% end

%% Perform the clustering

    [idx,C,sumd,D,midx,info] = kmedoids(L,k,'Distance','euclidean');

    %% Calculation of scaling factors

    count = ones(k,1); % preallocation for speed
    store_scalingsys=[];

    for i = 1:1:k

        enum_h = sum(sum(L(idx==i,1:24)));
        denom_h = sum(C(i,1:24)) * sum(idx == i);
        enum_c = sum(sum(L(idx==i,25:48)));
        denom_c = sum(C(i,25:48)) * sum(idx == i);
        enum_e = sum(sum(L(idx==i,49:72)));
        denom_e = sum(C(i,49:72)) * sum(idx == i);

        C(i,1:24) = C(i,1:24) * (enum_h / denom_h);
        C(i,25:48) = C(i,25:48) * (enum_c / denom_c);
        C(i,49:72) = C(i,49:72) * (enum_e / denom_e);

        count(i) = sum(idx == i); % How many days each typical day represents
        store_scalingsys=[store_scalingsys,[enum_h;denom_h;enum_c;denom_c;enum_e;denom_e]];

    end
    C(isnan(C)) = 0;
    typical_days = C; %; [C;Heat_peak_profile; Cool_peak_profile; Elec_peak_profile]

    typical_days(isnan(typical_days)) = 0;

    typical_days_heat = reshape(typical_days(:,1:24)',24*(k),1); %k:typical_days_heat = reshape(typical_days(:,1:24)',24*(k+3),1);
    typical_days_cool = reshape(typical_days(:,25:48)',24*(k),1);
    typical_days_elec = reshape(typical_days(:,49:72)',24*(k),1);

    typical_days_monthly=[typical_days_monthly;typical_days];
    

    %% Compute error in Load Duration Curve between clustered and original data
    % in selection of typical days package
    %
    % Required parameters:
    % - orginal data
    % - count 
    % - series
    % - typical_days (L = horzcat(Heat, Cool, Elec);)


    % %% Load duration curve calculation for clustered data
    % % Reconstitue full year from clustered data
    % typical_days_year=typical_days;
    % for i = 1:1:k
    %     size_cluster = count(i,1);
    %     for j = 1:1:size_cluster-1
    %         typical_days_year=[typical_days_year;typical_days(i,:)];
    %     end
    % end
    % 
    % %Respective load factor
    % Heating_typical_days = reshape(typical_days_year(:,1:24),1,8760)';
    % Cooling_typical_days = reshape(typical_days_year(:,25:48),1,8760)';                 % number of clusters = k + 3(heat/cooling/electricity peaks)
    % Elec_typical_days = reshape(typical_days_year(:,49:72),1,8760)';
    % 
    % 
    % 
    % % Reconstiture original data 
    % % data_original = [L;Heat_peak_profile;Cool_peak_profile;Elec_peak_profile];
    % % 
    % % Heating_typical_day = reshape(typical_days(:,1:24),1,(k+3)*24);
    % % Cooling_typical_day = reshape(typical_days(:,25:48),1,(k+3)*24);                 % number of clusters = k + 3(heat/cooling/electricity peaks)
    % % Elec_typical_day = reshape(typical_days(:,49:72),1,(k+3)*24);
    % 
    % 
    % % Sort data
    % %Clustered data
    % load_c_heat = sort(Heating_typical_days,'descend');
    % load_c_cool = sort(Cooling_typical_days,'descend');
    % load_c_elec = sort(Elec_typical_days,'descend');
    % 
    % %Orginal data
    % load_o_heat = sort(Heat_vec,'descend');
    % load_o_cool = sort(Cool_vec,'descend');
    % load_o_elec = sort(Elec_vec,'descend');
    % 
    % %% PLOT LDC
    % % heat_plot = duration_plot([Heat_vec, Heating_typical_days],'y_label','Heating demand_matlab');
    % % cool_plot = duration_plot([Cool_vec, Cooling_typical_days],'y_label','Cooling demand_matlab');
    % % elec_plot = duration_plot([Elec_vec, Elec_typical_days],'y_label','Electricity demand_matlab');
    % 
    % 
    % %% Error in load duration curve calculation: HEATING
    % eldc_heat = sum(abs(load_o_heat-load_c_heat))/sum(load_o_heat);
    % 
    % %% Error in load duration curve calculation: COOLING
    % eldc_cool = sum(abs(load_o_cool-load_c_cool))/sum(load_o_cool);
    % 
    % %% Error in load duration curve calculation: ELECTRICITY
    % eldc_elec = sum(abs(load_o_elec-load_c_elec))/sum(load_o_elec);
    % 
    % eldc = eldc_heat + eldc_cool + eldc_elec;
    % 
    % eldc_saved = [eldc_saved;eldc];
    % 
    % 
    % %% Compute error in Load Duration Curve between clustered and original data
    % % in selection of typical days package
    % %
    % % Required parameters:
    % % - D: Distance matrix
    % % - C: Typical days matrix
    % % - k: number of clusters (without peak days)
    % 
    % 
    % %% Dispersion measure of Cluster i : Si
    % q=2;     % Euclidean distance 
    % S=zeros(1,k);
    % 
    % for i = 1:1:k
    %     %D is an n-by-k matrix, where element (j,m) is the distance from observation j to medoid m.
    %     % sumd=sum(D(idx==i,i))
    %     S(1,i)=((1/sum(idx == i))*sum((D(idx==i,i)).^q))^(1/q);
    % end
    % 
    % %% Distance between medoids
    % t=2;     % Euclidean distance
    % M=zeros(k,k); % Medoids center matrix
    % R=zeros(k,k); % Compactness matrix between two clusters
    % MaxR=zeros(1,k);
    % 
    % for i=1:1:k
    %     for j=1:1:k
    %         if i==j
    %             M(i,j)=1;
    %             R(i,j)=0;
    %         else
    %             M(i,j)=(sum((abs(C(i,1:72)-C(j,1:72))).^t))^(1/t);
    %             R(i,j)=(S(1,i)+S(1,j))/M(i,j);
    %         end
    %     end
    %     MaxR(1,i)=max(R(i,:));
    % end
    % 
    % i_db=(1/k)*sum(MaxR);
    % 
    % i_db_saved=[i_db_saved;i_db];
    % 
    % % end
    % % figure
    % % plot(1:1:kmax, eldc_saved)
    % % 
    % % figure
    % % plot(1:1:kmax, i_db_saved)
    % 
    % typical__vs_original_days = [typical_days_heat; typical_days_cool; typical_days_elec; Heat_vec; Cool_vec; Elec_vec];

end
end